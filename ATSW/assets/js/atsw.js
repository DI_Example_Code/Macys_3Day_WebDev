const apiBase = "https://swapi.co/api/";
var categories, dataSet, selectedCat;
var dataMap = {
   "people": ["name", "gender", "birth_year", "species", "skin_color", "hair_color", "eye_color", "homeworld"],
   "planets": ["name", "orbital_period", "rotation_period", "climate", "diameter", "population", "terrain", "surface_water"],
   "films": ["title", "episode_id", "director", "release_date"],
   "species": ["name", "classification", "designation", "language", "average_height", "average_lifespan", "skin_colors", "hair_colors", "eye_colors"],
   "vehicles": ["name", "model", "manufacturer", "vehicle_class", "length", "cargo_capacity", "passengers"],
   "starships": ["name", "model", "manufacturer", "length", "hyperdrive_rating", "cargo_capacity", "crew"]
}

function getData(category) {
   selectedCat = category;

	$.getJSON(apiBase + category, function(data) {
      dataSet = data.results;

      clearDataPanels();

      fillResultListFromData (dataSet);
		
      $('#recordCount').text(`${data.count} records found`);
	});
}

function highlightMenuItem(anItem) {
   $('.menuItem').css({"color": "gray"});
   anItem.css({"color": "#FF6"});
}

function clearDataPanels() {
   $("section aside").fadeOut(0).empty();
   $("section article").empty();
}

function fillResultListFromData(dataArray) {
   var accessor = selectedCat == 'films' ? 'title' : 'name';
   var classStr;
   var dataObj;

   for (var i = 0; i < dataArray.length; i++) {
      // Select the first row
      dataObj = $(`<div class="dataObject" value="${i}">${dataArray[i][accessor]}</div>`);
      dataObj.click(_handleSelected);

      if (i == 0) {
         selectListObject(dataObj);
      }

      $("section article").append(dataObj);
   }
   
   $("section aside").fadeIn();
}

function fillDetailPanel(data) {
   var accessors = dataMap[selectedCat];
   var name;

   $("#mainSection aside").empty();

   for (var i = 0; i < accessors.length; i++) {
      name = accessors[i];

      $("#mainSection aside").append(`<div><label>${capitalizeEachWord(name)}:</label> ${data[name]}</div>`);
   }

   $("section aside").fadeIn();
}

function _handleSelected() {
   selectListObject($(this));
}

function selectListObject(aJQueryObj) {
   $(".dataObject").removeClass("selectedRow");

   aJQueryObj.addClass("selectedRow");

   fillDetailPanel(dataSet[aJQueryObj.attr("value")]);
}

function capitalizeEachWord(str) {
   return str.replace("_", " ").replace(/\w\S*/g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
   });
}

$(document).ready(function() {
   // Setup the error panel
   $("#errorPanel").css({"display": "flex"});
   $("#errorPanel").hide();

   // Populate the menu with the categories the SWAPI provides
   $.getJSON(apiBase, function(data) {
      categories = Object.keys(data);

      // Add each category as a menu item in the nav
      for (var i = 0; i < categories.length; i++) {
         $('#menuContainer').append(`<div class='menuItem' value='${categories[i]}'>${categories[i]}</div>`);
         $('#categoryList').append(`<option value='${categories[i]}'>${categories[i]}</option>`);
      }

      // Add a click handler to the menu item
      $(".menuItem").click(function() {
         clearDataPanels();
         highlightMenuItem($(this));

         selectedCat = $(this).attr("value");

         $("#categoryList").val(selectedCat);

      	getData(selectedCat);
      });
   });

   // Handle the search "form"
   $("#searchButton").click(function() {
      selectedCat = $("#categoryList").val();

      var url = (apiBase + selectedCat + "/" + $("#idInput").val()).toLowerCase();

      clearDataPanels();
      
      $.getJSON(url, function(data) {
         dataSet = data.results;

         // Sync the menu with the list selection
         highlightMenuItem($(`.menuItem[value=${selectedCat}]`))

         if ("count" in data) {
            // Button hit with no ID specified
            fillResultListFromData(dataSet);
            
            // Update the record count in the footer
            $('#recordCount').text(`${data.count} records found`);
         }
         else {
            // A single record was found
            fillResultListFromData([data]);

            // Update the record count in the footer
            $('#recordCount').text("1 record found");
         }
      }).fail(function(e) {
         if (e.responseText.indexOf("Not found") != -1) {
            $("#errorBanner h1").html("Object not found in SWAPI");
            $("#errorPanel").fadeIn('slow', function () {
               $(this).delay(1000).fadeOut('slow');
            });
         }
      });
   });
});