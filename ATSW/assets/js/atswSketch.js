const apiBase = "https://swapi.co/api/";

function getData(category) {
	$.getJSON(apiBase + category, function(data) {
		$('#mainSection').text(JSON.stringify(data));
      $('#recordCount').text(data.count + " records found");
		console.log(`${data.count} results...`);
	});
}

$(document).ready(function() {
   $("#searchButton").click(function() {
      var url = apiBase + $("#categoryList").val() + "/" + $("#idInput").val();
      url = url.toLowerCase();
      
      $.getJSON(url, function(data) {
         $("#mainSection").text(JSON.stringify(data));
         $('#recordCount').text(data.count + " records found");
      }).fail(function(e) {
         if (e.responseText.indexOf("Not found") != -1) {
            $("#contentDiv").empty();
            $("#errorDiv").html("Object not found in Repository of All Things Star Wars");
         }
      });
   });
});